import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Welcome to Flutter',
      home: Scaffold(
        appBar: AppBar(title: Text("Welcome to Flutter"),),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Hello World", style: TextStyle(color: Colors.blue),),
              SizedBox(height: 10,),
              Text("Name: Hugo Janasik", style: TextStyle(color: Colors.red),),
              SizedBox(height: 10,),
              Text("Graduation Date: Spring 2021", style: TextStyle(color: Colors.green),),
              SizedBox(height: 10,),
              Text("Favorite Quote: Practice makes perfect", style: TextStyle(fontStyle: FontStyle.italic, color: Colors.orange),)
            ],
          ),
        ),
      ),
    );
  }
}
